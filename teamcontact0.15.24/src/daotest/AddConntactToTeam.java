package daotest;

import dao.ContactService;
import dao.DAOImpl;
import dao.TeamService;
import hibernate.HibernateUtil;
import model.Contact;
import model.Team;

public class AddConntactToTeam {
	
	public static void main(String[] args) {

	
		DAOImpl<Team> teamdao = new TeamService();
		ContactService contactdao = new ContactService();
		TeamService temdaoimpl = new TeamService();

		Team t1 = temdaoimpl.findTerminByDesignation("TEAM1");
		
		
		Contact c1 = new Contact( "Bernd", "Lehmann", "Private","JavaDOZ", "bernd@web.de", "03071545827", "015545728885");
		Contact c2 = new Contact(  "Oliver", "Peter", "Business","PhytonDOZ", "oli@web.de", "03071495862", "015524657386");
		Contact c3 = new Contact(  "Lars", "Mueller", "Business","DBDOZ", "lars@web.de", "03071545827", "01908908870");
		
		
		t1.addContact(c1);
		t1.addContact(c2);
		t1.addContact(c3);
		
	//	contactdao.persist(c1);
		teamdao.merge(t1);
//		contactdao.removeContact(c1);
		
		System.out.println(contactdao.findAll(Contact.class));
		HibernateUtil.shutdown();
	}

}
