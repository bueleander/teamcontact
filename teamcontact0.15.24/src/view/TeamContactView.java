package view;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.logging.Logger;

import dao.ContactService;
import model.Contact;
import model.Team;

//Funktioniert nich im Application Server.
// Ohne omnifaces funst das nicht
//Workaround
//Sonst SessionScope
@SessionScoped
@Named
public class TeamContactView implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * @Purpose :Member variable to delete contact
	 */
	private Contact deleteContact;

	/**
	 * Member variable to select contact
	 */
	private Contact selectedContact;

	/**
	 * @Purpose :Create a new Contact Object
	 */
	private Contact newContact = new Contact();

	/**
	 * TODO
	 * @Purpose :Logging information about create/edit/delete contacts
	 */
	private static Logger logger = Logger.getLogger(TeamContactView.class);

	private String description = "Teams";

	/**
	 * @Purpose :To get Teams
	 */
	private List<Team> teamList = new ArrayList<>();

	/**
	 * @Purpose :To get all contacts and Teams
	 */
	private List<Contact> contactlist = new ArrayList<>();

	/**
	 *@Purpose :CDI from the service-class
	 */
	@Inject
	private ContactService contactDAO;

	
	public String createContact() {
		logger.info("Entering the creatContact method");
		String returnValue = "";
		
		FacesContext currentInstance = FacesContext.getCurrentInstance();
		
		try {
			
//			createContact();
			contactDAO.persist(newContact);
			
			logger.info("Save successful " + newContact.toString());

			// what is the message that we want to show?
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_INFO, "Saved", "Contact Saved");
			// display the message
			
			currentInstance.addMessage(null, fm);
			
		} catch (Exception e) {
			
			e.printStackTrace();
			returnValue = "fail";
			
			// what is the message that we want to show?
			FacesMessage fm = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Unable to Save", "Contact not Saved");
			// display the message
			currentInstance.addMessage(null, fm);
		}
		
		return returnValue;
		
		
	}

	public List<Contact> getContactlist() {
		return contactlist;
	}

	public void setContactlist(List<Contact> contactlist) {
		this.contactlist = contactlist;
	}

	public List<Team> getTeamList() {
		return teamList;
	}

	public void setTeamList(List<Team> teamList) {
		this.teamList = teamList;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public Contact getSelectedContact() {
		return selectedContact;
	}

	public void setSelectedContact(Contact selectedContact) {
		this.selectedContact = selectedContact;
	}

	public Contact getNewContact() {
		return newContact;
	}

	public void setNewContact(Contact newContact) {
		this.newContact = newContact;
	}

	public Contact getDeleteContact() {
		return deleteContact;
	}

	public void setDeleteContact(Contact deleteContact) {
		this.deleteContact = deleteContact;
	}
	
	/**
	 * To get all Contacts and add to ContactList page
	 * 
	 */
	@PostConstruct
	public void init() {
		logger.info("init...");

		setContactlist(contactDAO.findAll(Contact.class));

	}

	public void save() {
		System.out.println("save...");
	}

	/**
	 * @purpose Events
	 * Not used
	 */
	public void selectContactAction() {

		System.out.println("select " + selectedContact);
	}

	/**
	 * @Purpose :Create contact before adding to the contactlist
	 */
//	public void createContact() {
//
//		contactDAO.persist(newContact);
//	}

	/**
	 * @Purpose :For editing one selected contact from contactList
	 */
	public void updateContact() {

		contactDAO.merge(selectedContact);
	}

	/**
	 * @Purpose :For deleting one selected contact from contactList
	 */
	public void deleteContact() {

		contactDAO.remove(selectedContact);
	}

	/**
	 * @Purpose :Method to load the changes from the DB before redirect to the Contactlist page 
	 * @return 
	 */
	public String load() {

//		setNewContact(new Contact());

		setContactlist(contactDAO.findAll(Contact.class));

		return "ContactList";
	}

}
