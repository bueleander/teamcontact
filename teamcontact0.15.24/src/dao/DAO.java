package dao;

import java.util.List;

public interface DAO<T> {

	/**
	 * CRUD
	 * @Purpose :Save object of type T
	 * @param transientInstance
	 */
	public Object persist(final T transientInstance);

	/**
	 * @Purpose :Delete object of type T
	 * @param persistentInstance
	 */
	public void remove(final T persistentInstance);

	/**
	 * @Purpose :Update Object of type T
	 * @param detachedInstance
	 * @return
	 */
	public Object merge(final T detachedInstance);

	/**
	 * @Purpose :Find object by 'id' of type T
	 * @param identifier
	 * @return
	 */
	public T findById(final Integer identifier, Class<?> persistClass);
	
	
	
	/**
	 * @Purpose :Find all T
	 * @param identifier
	 * @return
	 */
	public List<T> findAll(Class<?> clazz);

}
