package dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import hibernate.HibernateUtil;



public class DAOImpl<T> implements DAO<T> {

	private SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
	
//	private static Logger logger = Logger.getLogger(TeamContactView.class);

	/**
	 * Generic
	 * @Purpose :Save object of type T
	 * @param transientInstance
	 */
	public Object persist(T entity) {
		getSession().beginTransaction();
		Object o = this.getSession().save(entity);
		getSession().getTransaction().commit();
		return o;
	}

	
	/**
	 * @Purpose :Delete object of type T
	 * @param persistentInstance
	 */
	@Override
	public void remove(T persistentInstance) {
		getSession().beginTransaction();
		this.getSession().delete(persistentInstance);
		getSession().getTransaction().commit();
	}

	/**
	 * @Purpose :Update Object of type T
	 * @param detachedInstance
	 * @return
	 */
	@Override
	//�ndert die zu �nder m�glichen Daten au�er di ID und merkt es. merge();
	public Object merge(T detachedInstance) {
		getSession().beginTransaction();
		Object o = this.getSession().merge(detachedInstance);
		getSession().getTransaction().commit();
		return o;
	}

	/**
	 * @Purpose :Find object by 'id' of type T
	 * @param identifier
	 * @return
	 */
	// Wenn in Listen nicht auf Typsicherheit gepr�ft werden
	@SuppressWarnings("unchecked")
	@Override
	// T Klassendefinition: Beliebiger Typ
	// Class<?>: Will einen Datentypen nutzen aber weis erst zur 
	// Laufzeit welcher typ kommt, wenn ein Typ kommt dann ist der Typsicher
	// Mit Generics nur eine DAOIml
	public T findById(Integer identifier, Class<?> persistClass) {
		getSession().beginTransaction();
		T o = (T) this.getSession().get(persistClass, identifier);
		
		getSession().getTransaction().commit();
		
		return o; 
		//this.getSession() = Select
	}

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}
//try
	public Session getSession() {
		return getSessionFactory().getCurrentSession();
	}

	/**
	 * @Purpose :Find all T
	 * @param identifier
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<T> findAll(Class<?> clazz) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		List<T> objects = session.createQuery("From " + clazz.getName()).getResultList();
		session.close();
		return objects;

	}
	

}
