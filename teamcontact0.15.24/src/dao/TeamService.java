package dao;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.TypedQuery;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import hibernate.HibernateUtil;
import model.Team;

@ApplicationScoped
public class TeamService extends DAOImpl<Team> {

	public Team getTeamById(Integer teamId) {
		// DAOImpl
		return findById(teamId, Team.class);
	}

	public Team findTerminByDesignation(String name) {
		Team t = null;
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {

			TypedQuery<Team> query = session.createQuery("FROM Team WHERE  designation=:des", Team.class);

			query.setParameter("des", name);

			t = query.getSingleResult();
		} catch (HibernateException e) {
			e.printStackTrace();

		} finally {
			session.close();
		}

		return t;

	}

}
