package dao;

import javax.enterprise.context.ApplicationScoped;
import javax.persistence.TypedQuery;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import hibernate.HibernateUtil;
import model.Contact;


@ApplicationScoped
public class ContactService extends DAOImpl<Contact> {

	public Contact getContactById(Integer contactId) {
		return findById(contactId, Contact.class);
	}

	/**
	 * - Objekt laden -get/load - neu setzen - save oder sql UPDATE
	 * 
	 * @param id
	 * @param field
	 * @param newValue
	 * @return
	 */
//	public Contact update(int id, String field, String newValue) {
//
//		return null;
//	}

	/**
	 * This method is used insert Address object by using HQL (Hibernate Query
	 * Language).
	 * 
	 * @param addressId
	 * @return
	 */

	public int updateContact(int id, String field, String newValue) {
		int result = -1;
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			@SuppressWarnings("unchecked")
			TypedQuery<Contact> query = session
					.createQuery("update Contact set " + field + "   =:nValue" + " where id=:id;");

			query.setParameter(newValue, "nValue");
			query.setParameter(id, "id");
			result = query.executeUpdate();
		} catch (HibernateException e) {
			e.printStackTrace();
			return result;
		} finally {
			session.close();
		}
		return result;
	}

	
	public int updateContact(String email, String field, String newValue) {
		int result = -1;
		Session session = HibernateUtil.getSessionFactory().openSession();
		try {
			session.beginTransaction();
			@SuppressWarnings("unchecked")
			TypedQuery<Contact> query = session
					.createQuery("update Contact set " + field + "   =:nValue" + " where email=:email");

			query.setParameter( "nValue",newValue);
			query.setParameter( "email",email);
			result = query.executeUpdate();
			session.getTransaction().commit();
		} catch (HibernateException e) {
			e.printStackTrace();
			return result;
		} finally {
			session.close();
		}
		return result;
	}

}
