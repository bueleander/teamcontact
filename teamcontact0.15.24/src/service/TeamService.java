package service;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import dao.DAOImpl;

import model.Contact;
import model.Team;

@ApplicationScoped
public class TeamService {
//	@Inject
//	DAOImpl<Contact> memdao;
	@Inject
	DAOImpl<Team> teamdao;
	@Inject
	DAOImpl<Contact> contactdao;

	public void createDefaultContacts() {
		List<Contact> contacts = contactdao.findAll(Contact.class);

		if (contacts.size() != 0)
			return;

//		contactdao.persist(new Contact(1, 1, "Georg", "Schmidt", "Business","georg@web.de","0307544545","015447885"));
//		contactdao.persist(new Contact(2, 2, "Hans","Bauer","Business","hans@web.de","0307558641","0155623928"));
//		contactdao.persist(new Contact(3, 2, "Peter","Born","Private","peter@web.de","0307578469","015538629"));
	}

	public void createDefaultTeams() {
		List<Team> teams = teamdao.findAll(Team.class);

		if (teams.size() != 0)
			return;

		teamdao.persist(new Team(1,"TEAM1", "1.1.2001", ""));
		teamdao.persist(new Team(2,"TEAM2", "2.2.2001", ""));
		teamdao.persist(new Team(3,"TEAM3", "3.3.2001", ""));

	}

//	public void createDefaultMembers() {
//		List<Contact> members = memdao.findAll(Contact.class);
//
//		if (members.size() != 0)
//			return;

//		memdao.persist(new Contact("Buelent", "Akcay", "bu@web.de")); Funzt nicht mehr

//		teamdao.persist(new Team("TEAM1", "1.1.2001", ""));
//		teamdao.persist(new Team("TEAM2", "2.2.2001", ""));
//		teamdao.persist(new Team("TEAM3", "3.3.2001", ""));

	}

