package model;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.enterprise.inject.Model;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity
@Model
public class Team implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Column(name="id_team")
	private int id;
	
	@Column(name="designation")
	private String designation;
	@Column(name="start_date")
	private String startDate;
	@Column(name="end_date")
	private String endDate;

	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "team_contacts", joinColumns = { @JoinColumn(name = "id_team") }, inverseJoinColumns = {
			@JoinColumn(name = "id_contact") })
	
	/**
	 * Team get Contacts
	 * @param contacts
	 */
	private Set<Contact> contacts = new HashSet<>();

	
	public Team() {

	}

	public Team(int id, String designation, String startDate, String endDate) {
		super();
		this.id = id;
		this.designation = designation;
		this.startDate = startDate;
		this.endDate = endDate;

	}

	public Team(String designation, String startDate, String endDate, Set<Contact> contacts) {
		super();
		this.designation = designation;
		this.startDate = startDate;
		this.endDate = endDate;
		this.contacts = contacts;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public Set<Contact> getContacts() {
		return contacts;
	}

	public void setContacts(Set<Contact> contacts) {
		this.contacts = contacts;
	}

	public void addContact(Contact team) {
		contacts.add(team);
	}

	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((contacts == null) ? 0 : contacts.hashCode());
		result = prime * result + ((designation == null) ? 0 : designation.hashCode());
		result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
		result = prime * result + id;
		result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Team other = (Team) obj;
		if (contacts == null) {
			if (other.contacts != null)
				return false;
		} else if (!contacts.equals(other.contacts))
			return false;
		if (designation == null) {
			if (other.designation != null)
				return false;
		} else if (!designation.equals(other.designation))
			return false;
		if (endDate == null) {
			if (other.endDate != null)
				return false;
		} else if (!endDate.equals(other.endDate))
			return false;
		if (id != other.id)
			return false;
		if (startDate == null) {
			if (other.startDate != null)
				return false;
		} else if (!startDate.equals(other.startDate))
			return false;
		return true;
	}
	

	@Override
	public String toString() {
		return "Team [id=" + id + ", designation=" + designation + ", startDate=" + startDate + ", endDate=" + endDate
				+ ", contacts=" + contacts + "]";
	}
}
