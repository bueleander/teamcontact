package model;

import java.io.Serializable;

import javax.enterprise.inject.Model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
@Model
public class Contact implements Serializable {

	private static final long serialVersionUID = 1L;
	
	
	
	@Id
	@Column(name="id_contact")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name="first_name")
	private String firstName;
	@Column(name="last_name")
	private String lastName;
	@Column(name="contact_type")
	private String contactType;
	@Column(name="job_titel")
	private String jobTitel;
	@Column(unique=true)
	private String eMail;
	@Column(name="phone_number_work")
	private String phoneNumberWork;
	@Column(name="phone_number_mobile")
	private String phoneNumberMobile;


	public Contact() {
		
	}

//														
	public Contact(int id, String firstName, String lastName,String contactType,String jobTitel, String eMail,
			String phoneNumberWork, String phoneNumberMobile) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.contactType = contactType;
		this.jobTitel = jobTitel;
		this.eMail = eMail;
		this.phoneNumberWork = phoneNumberWork;
		this.phoneNumberMobile = phoneNumberMobile;
	}


	public Contact(String firstName, String lastName, String contactType,String jobTitel, String eMail,
			String phoneNumberWork, String phoneNumberMobile) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.contactType = contactType;
		this.jobTitel = jobTitel;
		this.eMail = eMail;
		this.phoneNumberWork = phoneNumberWork;
		this.phoneNumberMobile = phoneNumberMobile;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getContactType() {
		return contactType;
	}

	public void setContactType(String contactType) {
		this.contactType = contactType;
		
	}

	public String getJobTitel() {
		return jobTitel;
	}

	public void setJobTitel(String jobTitel) {
		this.jobTitel = jobTitel;
	}

	public String geteMail() {
		return eMail;
	}

	public void seteMail(String eMail) {
		this.eMail = eMail;
	}

	public String getPhoneNumberWork() {
		return phoneNumberWork;
	}

	public void setPhoneNumberWork(String phoneNumberWork) {
		this.phoneNumberWork = phoneNumberWork;
	}

	public String getPhoneNumberMobile() {
		return phoneNumberMobile;
	}

	public void setPhoneNumberMobile(String phoneNumberMobile) {
		this.phoneNumberMobile = phoneNumberMobile;
	}
	
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((contactType == null) ? 0 : contactType.hashCode());
		result = prime * result + ((eMail == null) ? 0 : eMail.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + id;
		result = prime * result + ((jobTitel == null) ? 0 : jobTitel.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((phoneNumberMobile == null) ? 0 : phoneNumberMobile.hashCode());
		result = prime * result + ((phoneNumberWork == null) ? 0 : phoneNumberWork.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Contact other = (Contact) obj;
		if (contactType == null) {
			if (other.contactType != null)
				return false;
		} else if (!contactType.equals(other.contactType))
			return false;
		if (eMail == null) {
			if (other.eMail != null)
				return false;
		} else if (!eMail.equals(other.eMail))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (id != other.id)
			return false;
		if (jobTitel == null) {
			if (other.jobTitel != null)
				return false;
		} else if (!jobTitel.equals(other.jobTitel))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (phoneNumberMobile == null) {
			if (other.phoneNumberMobile != null)
				return false;
		} else if (!phoneNumberMobile.equals(other.phoneNumberMobile))
			return false;
		if (phoneNumberWork == null) {
			if (other.phoneNumberWork != null)
				return false;
		} else if (!phoneNumberWork.equals(other.phoneNumberWork))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Contact [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", contactType="
				+ contactType + ", jobTitel=" + jobTitel + ", eMail=" + eMail + ", phoneNumberWork=" + phoneNumberWork
				+ ", phoneNumberMobile=" + phoneNumberMobile + "]";
	}



	

}
